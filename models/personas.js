const Persona = require("./persona");
let personasDB = require('../db/data.json');

class Personas {
  constructor() {
    this._listado = [];
  }

  crearPersona(persona = {}) {

    this._listado[persona.id] = persona;
  }

  get listArray() {

    const listado = [];
    Object.keys(this._listado).forEach(key => {
      const persona = this._listado[key];
      listado.push(persona);
    })
    return listado;
  }

  cargarPersonasFromArray(personas = []) {
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    })
  }

  eliminarPersona(id = '') {
    if (this._listado[id]) {
      delete this._listado[id];
    }
  }

  buscarNombreApellido(nombres, apellidos) {
    let listado = [];
    listado = personasDB.filter(persona => {
      return persona.nombres == nombres && persona.apellidos == apellidos      
    })
    return listado;
  }

  buscarCi(ci) {
    let listado = [];
    listado = personasDB.filter(persona => persona.ci == ci);
    return listado;
  }

  buscarSexo(sexo) {
    let listado = [];
    listado = personasDB.filter(persona => persona.sexo == sexo);
    return listado;
  }

}

module.exports = Personas