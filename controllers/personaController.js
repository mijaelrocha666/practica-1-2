const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Persona = require('../models/persona');
const Personas = require('../models/personas');
const { request, response } = 'express';

const personas = new Personas();

let personaDB = leerDB();
if (personaDB) {
  personas.cargarPersonasFromArray(personaDB);
}

const personaGet = (req = request, res = response) => {

  let { nombres, apellidos } = req.query;

  if (nombres && apellidos) {
    let mostrarNomApe = personas.buscarNombreApellido(nombres, apellidos)
    res.json({
      mostrarNomApe
    })
    
  } else {
    res.json({
      personaDB
    })
  }
}

const Datos = (req = request, res= response) => {  
  let parametro = req.params.parametro;

  if (parseInt(parametro)) {
    let mostrarCi = personas.buscarCi(parametro);

    res.json({
      mostrarCi
    })

  } else {
    let mostrarSexo = personas.buscarSexo(parametro);

    res.json({
      mostrarSexo
    })
  }
}

const personaPut = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    const { nombres, apellidos, ci, direccion, sexo } = req.body;
    const persona = new Persona(nombres, apellidos, ci , direccion, sexo);
    persona.getId(id)
    personas.crearPersona(persona);
    guardarDB(personas.listArray);
    personaDB = leerDB()
  }


  res.json({
    message: 'put API - Controlador',
    personaDB
  })
}



const personaPost = (req, res = response) => {

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  const persona = new Persona(nombres, apellidos, ci, direccion, sexo);

  personas.crearPersona(persona);
  guardarDB(personas.listArray);

  const listado = leerDB();
  personas.cargarPersonasFromArray(listado)

  res.json({
    message: 'post API - Controlador',
    listado
  })
}


const personaDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    personas.eliminarPersona(id);
    guardarDB(personas.listArray)

// para recuperar datos
    personaDB = leerDB()
  }

  res.json({
    message: 'delete API - Controlador',
    personaDB
  })
}

module.exports = {
  personaGet,
  Datos,
  personaPut,
  personaPost,
  personaDelete
}