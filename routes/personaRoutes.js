const { Router } = require('express');
const { personaGet, personaPut, personaPost, personaDelete, Datos } = require('../controllers/personaController')

const router = Router();

router.get('/', personaGet);
router.get('/:parametro', Datos);
router.put('/:id', personaPut);
router.post('/', personaPost);
router.delete('/:id', personaDelete);

module.exports = router;